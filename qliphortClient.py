import player
import le_map
import bombs
import _thread
import time
import socket
import pickle
import sys
import pygame

connected=True
glosocket=""

def receive_msg( qliphort, delay, playerData, playerData2, mapData):
        while True:
                try:
                        sep = 'o'
                        
                        data = qliphort.recv(37)
                        data = data.decode()
                        data = data.split('p')
                        playerData2.direction = int(data[0])
                        playerData2.standingAnimation()
                        #print (data[0].strip('o'), data[1].strip('o'))
                        
                        playerData2.position.y = int(data[1].strip('o'))
                        
                        playerData2.position.x = int(data[2].strip('o'))

                        bombY = data[3].strip('o')
                        
                        bombX = data[4].strip('o')

                        if bombY != '' and bombX != '':
                                playerData2.bombCount += 1
                                playerData2.bomb.append(bombs.Bomb(playerData2.screen))
                                playerData2.bomb[playerData2.bombCount - 1].initBomb(int(bombX),int(bombY),playerData2.firePower,pygame.time.get_ticks())
                        
                        data = qliphort.recv(332)
                        mapTotal = data
                        data = data.decode()
                        data = data.split('o')
                        data.pop()
                        yIndex = 0
                        counter = 0
                        
                        for x in data:
                                xIndex = 0
                                row = ""
                                for each in x:
                                        if each == "3" or each == "4":
                                                row += each
                                        else:
                                                row += mapData.coord[yIndex][0][xIndex]
                                        xIndex += 1
                                mapData.coord[yIndex][0] = row
                                yIndex += 1
                        playerData.mymap.checkPowerup(playerData)
                        playerData2.mymap.checkPowerup(playerData2)
                        
                        break
                except:
                        break


def send_msg( qliphort, delay, playerData, playerData2, mapData):
        global connected
        while True:
                try:
                        input_send = str(playerData.direction)
                        input_send += 'p'
                        sep = 'o'
                        
                        input_send += str(playerData.position.y)
                        for i in range(0, 8-len(str(playerData.position.y))):
                                input_send += sep

                        input_send += 'p'
                        input_send += str(playerData.position.x)
                        
                        for i in range(0, 8-len(str(playerData.position.x))):
                                input_send += sep

                        input_send += 'p'
                        if len(playerData.bombSend) != 0:
                                input_send += str(playerData.bombSend[0].position.y)
                                for i in range(0, 8-len(str(playerData.bombSend[0].position.y))):
                                        input_send += sep
                                        
                                input_send += 'p'

                                input_send += str(playerData.bombSend[0].position.x)
                                for i in range(0, 8-len(str(playerData.bombSend[0].position.x))):
                                        input_send += sep
                                playerData.bombSend.pop(0)
                        else:
                                input_send += 'oooooooopoooooooo' #buffer

                        qliphort.send(input_send.encode('UTF-8'))
                        
                        mapTotal = ''
                        temp = ''
                                        
                        for x in mapData.coord:
                                temp = ''.join(x)
                                temp += 'o'
                                mapTotal += temp

                        qliphort.send(mapTotal.encode('UTF-8'))
                        
                        receive_msg( qliphort, delay, playerData, playerData2, mapData)
                except:
                        break

def endClient( player):
        glosocket.close()
        if player.alive == False:
                print('You lose! Player 2 won!')
        else:
                print('You win! Player 2 lost!')
                

def start( playerData, playerData2, mapData, bombData):
        global glosocket
        s = socket.socket()
        #host = "203.189.120.150"
        host = socket.gethostname()
        port = 32135
        #s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        s.connect((host, port))

        connected=True
        data = s.recv(9)
        data = data.decode()
        if data != "123456789":
                data = data.split('p')
                imagepath = data[0].strip('o')
                
                if imagepath == "1":
                        playerData.model = pygame.image.load("assets/player1.png")
                        playerData2.model = pygame.image.load("assets/player2.png")
                        print('Connected! Waiting for Player 2..')
                        gomsg = s.recv(2)
                else:
                        playerData.model = pygame.image.load("assets/player2.png")
                        playerData2.model = pygame.image.load("assets/player1.png")
                        mapData.randoming = False
                        print('Connected! Waiting for Player 1..')
                        gomsg = s.recv(2)

                playerData.initModel()
                playerData2.initModel()
                        
                playerData.position.y = int(data[1].strip('o'))
                playerData.position.x = int(data[2].strip('o'))
                print('All players are ready! GO!')
                glosocket = s
                _thread.start_new_thread( send_msg, (s, 1, playerData, playerData2, mapData, ) )
                return 1
        else:
                print('Too bad, only 2 players can play at one time! In the meantime play FEH ^_^V')
                return 99
        
