import _thread
import time
import socket

connected=True

def qliphort_stabilizer( qliphort, delay):
        while connected:
                time.sleep(delay)
                try:
                        qliBuffer = " "
                        qliphort.send(qliBuffer.encode('UTF-8'))
                except:
                        print ('Not stable')
                        break
        qliphort.close()


def receive_msg( qliphort, delay):
        while connected:
                #print "Recv"
                #time.sleep(delay)
                try:
                        data = qliphort.recv(1024)
                        data = data.decode()
                        data = data.strip()
                        if data != "":
                                print ('Server: ', data)
                except:

                        break


def send_msg( qliphort, delay):
        global connected
        while True:
                #print "Send"
                #time.sleep(delay)
                input_send = input()
                if input_send == '-q':
                        connected=False
                        break
                try:
                        qliphort.send(input_send.encode('UTF-8'))
                except:
                        break


s = socket.socket()
host = socket.gethostname()
port = 32135

s.connect((host, port))

connected=True
_thread.start_new_thread( qliphort_stabilizer, (s, 1, ) )
_thread.start_new_thread( receive_msg, (s, 1, ) )
_thread.start_new_thread( send_msg, (s, 1, ) )

while connected:
        pass
