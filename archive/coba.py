import qliphortClient
import pygame
import player
import le_map
import bombs

pygame.init()
 
screen = pygame.display.set_mode((640, 480))
clock = pygame.time.Clock()
 

le_map = le_map.Map(screen)
bomb = bombs.Bomb(screen)
unit = player.Player(screen,le_map,bomb)

game_over = False

while game_over == False:
 
    for event in pygame.event.get():
        print (event)
        if event.type == pygame.QUIT:
            game_over = True
        unit.keyboardInput(event)
    
    screen.fill(pygame.Color('green'))  
    le_map.animate()
    unit.animate()
    pygame.display.flip()              
    clock.tick(10)
 
pygame.quit ()

