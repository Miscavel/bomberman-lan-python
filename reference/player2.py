import pygame
import bombs

class Player(pygame.sprite.Sprite):
        def __init__(self,screen,mymap,bomb,file):
                self.model = pygame.image.load(file)
                self.modelDown = []
                self.modelLeft = []
                self.modelRight = []
                self.modelUp = []
                self.directionStack = []
                self.mymap = mymap

                self.modelDown.append(self.model.subsurface(pygame.Rect(0,0,32,32)))
                self.modelDown.append(self.model.subsurface(pygame.Rect(32,0,32,32)))
                self.modelDown.append(self.model.subsurface(pygame.Rect(64,0,32,32)))

                self.modelLeft.append(self.model.subsurface(pygame.Rect(0,32,32,32)))
                self.modelLeft.append(self.model.subsurface(pygame.Rect(32,32,32,32)))
                self.modelLeft.append(self.model.subsurface(pygame.Rect(64,32,32,32)))

                self.modelRight.append(self.model.subsurface(pygame.Rect(0,64,32,32)))
                self.modelRight.append(self.model.subsurface(pygame.Rect(32,64,32,32)))
                self.modelRight.append(self.model.subsurface(pygame.Rect(64,64,32,32)))

                self.modelUp.append(self.model.subsurface(pygame.Rect(0,96,32,32)))
                self.modelUp.append(self.model.subsurface(pygame.Rect(32,96,32,32)))
                self.modelUp.append(self.model.subsurface(pygame.Rect(64,96,32,32)))

                self.modelNow = self.modelDown[0]
                self.position = self.modelNow.get_rect()
                self.position.x = 320
                self.position.y = 320
                self.screen = screen
                self.direction = "down"
                self.modelIndex = 0
                self.isMoving = False

                self.bombCount = 0
                self.maxBomb = 6
                self.firePower = 3;
                self.bomb = []
                self.bombSend = []
                        

        def update(self):
                self.mymap.checkPowerup(self)
                for i in self.bomb:
                        if i.isExploding == True:
                                if i.explodingAnimationTime < 4 :
                                        i.explodingAnimationTime += 1
                                else:
                                        self.bomb.remove(i)
                                        self.bombCount -= 1
                
        def animate(self):
                self.update()
                for i in self.bomb:
                        i.animate(self.mymap,self)
                self.screen.blit(self.modelNow, self.position)                     
                        
        def getDirection(self):
                for i in self.directionStack:
                        self.direction = i
                return self.direction




                
        
