import pygame
import random

class Map(pygame.sprite.Sprite):
        def __init__(self,screen):
                self.model = pygame.image.load("assets/wall.png")
                self.model2 = pygame.image.load("assets/floor.png")
                self.model3 = pygame.image.load("assets/block.png")
                self.firePowerup = pygame.image.load("assets/flame_powerup.png")
                self.bombPowerup = pygame.image.load("assets/bomb_powerup.png")
                
                self.wallModel = self.model.subsurface(pygame.Rect(0,0,32,32))
                self.floorModel = self.model2.subsurface(pygame.Rect(0,0,32,32))
                self.blockModel = self.model3.subsurface(pygame.Rect(0,0,32,32))
                self.firePowerupModel = self.firePowerup.subsurface(pygame.Rect(0,0,32,32))
                self.bombPowerupModel = self.bombPowerup.subsurface(pygame.Rect(0,0,32,32))
                
                self.mapCoord = {}
                                
                self.screen = screen
                self.randoming = True
                self.updating = False
                       
                f = open("assets/coord.txt","r")
                self.coord = []
               
                for line in f:
                        self.coord.append(line.split())

        def animate(self):
                yInc = 0

                for x in self.coord:
                        for i in x:
                                xInc = 0
                                for j in i :
                                        if j == "0":
                                                self.screen.blit(self.floorModel,(xInc,yInc))
                                        elif j == "1":
                                                self.screen.blit(self.wallModel,(xInc,yInc))
                                        elif j == "2" :
                                                self.screen.blit(self.blockModel,(xInc,yInc))
                                        elif j == "3":
                                                self.screen.blit(self.firePowerupModel,(xInc,yInc))
                                        elif j == "4" :
                                                self.screen.blit(self.bombPowerupModel,(xInc,yInc))
                                        xInc += 32
                        yInc += 32
                        
        def isCollideWall(self,player):
                xPos = int(player.position.x/32)
                yPos = int(player.position.y/32)
                if self.coord[yPos][0][xPos] == "1" or self.coord[yPos][0][xPos] == "2" :
                        return True
                else:
                        return False

        def isGameOver(self,player1,player2):
                x1 = player1.position.x
                y1 = player1.position.y

                x2 = player2.position.x
                y2 = player2.position.y
                
                for i in player1.bomb :
                        if  i.isExploding == True :
                                for j in range (1,i.firePower+1):
                                        if x1 == i.position.x-(32*j) and y1 == i.position.y :
                                                player1.alive = False
                                                return True
                                        if x1 == i.position.x+(32*j) and y1 == i.position.y :
                                                player1.alive = False
                                                return True
                                        if x1 == i.position.x and y1 == i.position.y-(32*j) :
                                                player1.alive = False
                                                return True
                                        if x1 == i.position.x and y1 == i.position.y+(32*j) :
                                                player1.alive = False
                                                return True

                                        if x2 == i.position.x-(32*j) and y2 == i.position.y :
                                                player2.alive = False
                                                return True
                                        if x2 == i.position.x+(32*j) and y2 == i.position.y :
                                                player2.alive = False
                                                return True
                                        if x2 == i.position.x and y2 == i.position.y-(32*j) :
                                                player2.alive = False
                                                return True
                                        if x2 == i.position.x and y2 == i.position.y+(32*j) :
                                                player2.alive = False
                                                return True
                for i in player2.bomb :
                        if  i.isExploding == True :
                                for j in range (1,i.firePower+1):
                                        if x1 == i.position.x-(32*j) and y1 == i.position.y :
                                                player1.alive = False
                                                return True
                                        if x1 == i.position.x+(32*j) and y1 == i.position.y :
                                                player1.alive = False
                                                return True
                                        if x1 == i.position.x and y1 == i.position.y-(32*j) :
                                                player1.alive = False
                                                return True
                                        if x1 == i.position.x and y1 == i.position.y+(32*j) :
                                                player1.alive = False
                                                return True

                                        if x2 == i.position.x-(32*j) and y2 == i.position.y :
                                                player2.alive = False
                                                return True
                                        if x2 == i.position.x+(32*j) and y2 == i.position.y :
                                                player2.alive = False
                                                return True
                                        if x2 == i.position.x and y2 == i.position.y-(32*j) :
                                                player2.alive = False
                                                return True
                                        if x2 == i.position.x and y2 == i.position.y+(32*j) :
                                                player2.alive = False
                                                return True

                return False
        
        def checkPowerup(self,player):
                xPos = int(player.position.x/32)
                yPos = int(player.position.y/32)
                if self.coord[yPos][0][xPos] == "3":
                        player.firePower += 1
                        temp = ""
                        index = 0
                        for i in self.coord[yPos][0]:
                                if index == xPos:
                                        temp += "0"
                                else:
                                        temp += i
                                index += 1
                        self.coord[yPos][0] = temp
                if self.coord[yPos][0][xPos] == "4":
                        player.maxBomb += 1
                        temp = ""
                        index = 0
                        for i in self.coord[yPos][0]:
                                if index == xPos:
                                        temp += "0"
                                else:
                                        temp += i
                                index += 1
                        self.coord[yPos][0] = temp

        def fireContinue(self,xPos,yPos,animTime,player):
                xPos = int(xPos/32)
                yPos = int(yPos/32)
                if self.coord[yPos][0][xPos] == "1" :
                        return False
                if self.coord[yPos][0][xPos] == "2" :
                        if animTime >= 4:
                                temp = ""
                                index = 0
                                for i in self.coord[yPos][0]:
                                        if index == xPos:
                                                if self.randoming == True:
                                                        roulete = random.randint(0,10)
                                                        if roulete == 3:
                                                                temp += "3"
                                                        elif roulete == 4:
                                                                temp += "4"
                                                        else:
                                                                temp += "0"
                                                else:
                                                        temp += "0"
                                        else:
                                                temp += i
                                        index += 1
                                self.coord[yPos][0] = temp
                        return False
                for i in player.bomb:
                        if i.position.x == xPos*32 and i.position.y == yPos *32 :
                                if animTime >= 4:
                                        i.isExploding = True
                                return False
                return True

        def debugMap(self):
                temp = ""
                check = 0
                for x in self.coord[0][0]:
                        if check == 0:
                                temp += "0"
                                check = 1
                        else:
                                temp += x
                self.coord[0][0] = temp
