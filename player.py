import pygame
import bombs

class Player(pygame.sprite.Sprite):
        def __init__(self,screen,mymap,bomb,file):
                self.model = pygame.image.load(file)
                self.modelDown = []
                self.modelLeft = []
                self.modelRight = []
                self.modelUp = []
                self.directionStack = []
                self.mymap = mymap
                
                self.screen = screen
                self.direction = 0
                # 0 - 3 = down , up , left ,right
                self.modelIndex = 0
                self.isMoving = False

                self.bombCount = 0
                self.maxBomb = 6
                self.firePower = 3;
                self.bomb = []
                self.bombSend = []
                self.alive = True

        def initModel(self):
                self.modelDown.append(self.model.subsurface(pygame.Rect(0,0,32,32)))
                self.modelDown.append(self.model.subsurface(pygame.Rect(32,0,32,32)))
                self.modelDown.append(self.model.subsurface(pygame.Rect(64,0,32,32)))

                self.modelLeft.append(self.model.subsurface(pygame.Rect(0,32,32,32)))
                self.modelLeft.append(self.model.subsurface(pygame.Rect(32,32,32,32)))
                self.modelLeft.append(self.model.subsurface(pygame.Rect(64,32,32,32)))

                self.modelRight.append(self.model.subsurface(pygame.Rect(0,64,32,32)))
                self.modelRight.append(self.model.subsurface(pygame.Rect(32,64,32,32)))
                self.modelRight.append(self.model.subsurface(pygame.Rect(64,64,32,32)))

                self.modelUp.append(self.model.subsurface(pygame.Rect(0,96,32,32)))
                self.modelUp.append(self.model.subsurface(pygame.Rect(32,96,32,32)))
                self.modelUp.append(self.model.subsurface(pygame.Rect(64,96,32,32)))

                self.modelNow = self.modelDown[0]
                self.position = self.modelNow.get_rect()

        def standingAnimation(self):
                if self.direction == 0 :
                        self.modelNow = self.modelDown[self.modelIndex]
                if self.direction == 1 :
                        self.modelNow = self.modelUp[self.modelIndex]
                if self.direction == 2 :
                        self.modelNow = self.modelLeft[self.modelIndex]
                if self.direction == 3 :
                        self.modelNow = self.modelRight[self.modelIndex]
                        
        def update(self):
                cek = 0
                for i in self.directionStack:
                        self.direction = i
                        cek = 1
                if cek == 0 :
                        self.isMoving = False
                else :
                        self.isMoving = True
                            
                if self.isMoving :                    
                        if self.direction == 0:
                            self.modelNow = self.modelDown[self.modelIndex]
                            self.position.y += 32

                        if self.direction == 1:
                            self.modelNow = self.modelUp[self.modelIndex]
                            self.position.y -= 32

                        if self.direction == 2:
                            self.modelNow = self.modelLeft[self.modelIndex]
                            self.position.x -= 32

                        if self.direction == 3:
                            self.modelNow = self.modelRight[self.modelIndex]
                            self.position.x += 32

                        if self.mymap.isCollideWall(self) == True:
                                 if self.direction == 0:
                                    self.modelNow = self.modelDown[self.modelIndex]
                                    self.position.y -= 32

                                 if self.direction == 1:
                                    self.modelNow = self.modelUp[self.modelIndex]
                                    self.position.y += 32

                                 if self.direction == 2:
                                    self.modelNow = self.modelLeft[self.modelIndex]
                                    self.position.x += 32

                                 if self.direction == 3:
                                    self.modelNow = self.modelRight[self.modelIndex]
                                    self.position.x -= 32
                else :
                        self.standingAnimation()
                self.mymap.checkPowerup(self)
            
                for i in self.bomb:
                        if i.isExploding == True:
                                if i.explodingAnimationTime < 4 :
                                        i.explodingAnimationTime += 1
                                else:
                                        self.bomb.remove(i)
                                        self.bombCount -= 1

                self.modelIndex += 1
                if self.modelIndex == 3:
                        self.modelIndex = 0
                
        def animate(self):
                self.update()
                for i in self.bomb:
                        i.animate(self.mymap,self)
                self.screen.blit(self.modelNow, self.position)
        
        def keyboardInput(self,inp):
                if inp.type == pygame.KEYDOWN:
                        if inp.key == pygame.K_DOWN:
                                self.direction = 0
                                self.directionStack.append(0)

                        if inp.key == pygame.K_UP:
                                self.direction = 1
                                self.directionStack.append(1)
                        
                        if inp.key == pygame.K_LEFT:
                                self.direction = 2
                                self.directionStack.append(2)
                                
                        if inp.key == pygame.K_RIGHT:
                                self.direction = 3
                                self.directionStack.append(3)
                                                              
                        if inp.key == pygame.K_z:
                                if self.bombCount < self.maxBomb:
                                        self.bombCount += 1
                                        self.bomb.append(bombs.Bomb(self.screen))
                                        self.bomb[self.bombCount - 1].initBomb(self.position.x,self.position.y,self.firePower,pygame.time.get_ticks())
                                        #socketing purpose
                                        self.bombSend.append(bombs.Bomb(self.screen))
                                        self.bombSend[len(self.bombSend)-1].initBomb(self.position.x,self.position.y,self.firePower,pygame.time.get_ticks())
                                
                if inp.type == pygame.KEYUP:
                        if inp.key == pygame.K_DOWN:
                               for i in self.directionStack:
                                        if i == 0:
                                                self.directionStack.remove(i)

                        if inp.key == pygame.K_UP:
                                for i in self.directionStack:
                                        if i == 1:
                                                self.directionStack.remove(i)

                        if inp.key == pygame.K_LEFT:
                                for i in self.directionStack:
                                        if i == 2:
                                                self.directionStack.remove(i)
                        if inp.key == pygame.K_RIGHT:
                                for i in self.directionStack:
                                        if i == 3:
                                                self.directionStack.remove(i)
                




                
        
