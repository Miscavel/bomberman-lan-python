import qliphortClient
import pygame
import player
import le_map
import bombs

pygame.init()

screen = pygame.display.set_mode((640, 480))
clock = pygame.time.Clock()

le_map = le_map.Map(screen)
bomb = bombs.Bomb(screen)
unit = player.Player(screen,le_map,bomb,"assets/player1.png")
unit2 = player.Player(screen,le_map,bomb,"assets/player2.png")

game_over = False

if qliphortClient.start(unit, unit2, le_map, bomb) == 1:
    while game_over == False:
        for event in pygame.event.get():
            #print (event)
            if event.type == pygame.QUIT:
                game_over = True
            unit.keyboardInput(event)

        screen.fill(pygame.Color('green'))

        le_map.animate()
        unit.animate()
        unit2.animate()
        
        if le_map.isGameOver(unit,unit2) == True:
            game_over = True
        pygame.display.flip()              
        clock.tick(10)

    qliphortClient.endClient(unit)
    pygame.quit ()
else:
    pygame.quit()
    pass



