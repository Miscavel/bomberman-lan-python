import pygame

class Bomb(pygame.sprite.Sprite):
    def __init__(self,screen):
        self.model = pygame.image.load("assets/bomb.png")
        self.modelFire = pygame.image.load("assets/flame.png")
        self.tickModel = self.model.subsurface(0,0,32,32)
        self.fireModel = self.modelFire.subsurface(0,0,31,32)
        self.position = self.tickModel.get_rect()
        self.screen = screen
        self.isExploding = False
        self.firePower = 0
        self.startTime = 0
        self.explodingAnimationTime = 0

    def animate(self,mymap,player):
        if self.startTime + 2000 > pygame.time.get_ticks() :
            self.screen.blit(self.tickModel,self.position)
        else :
            self.isExploding = True
        if self.isExploding == True:
            self.screen.blit(self.fireModel,self.position)
            contUp = True
            contDown = True
            contLeft = True
            contRight = True
            for i in range (1,self.firePower+1):
                if contLeft == True :
                    contLeft = mymap.fireContinue(self.position.x-(32*i),self.position.y,self.explodingAnimationTime,player)
                if contRight == True :
                    contRight = mymap.fireContinue(self.position.x+(32*i),self.position.y,self.explodingAnimationTime,player)
                if contUp == True :
                    contUp = mymap.fireContinue(self.position.x,self.position.y-(32*i),self.explodingAnimationTime,player)
                if contDown == True :
                    contDown = mymap.fireContinue(self.position.x,self.position.y+(32*i),self.explodingAnimationTime,player)

                if contLeft == True :
                    self.screen.blit(self.fireModel, (self.position.x-(32*i),self.position.y))
                if contRight == True :
                    self.screen.blit(self.fireModel, (self.position.x+(32*i),self.position.y))
                if contUp == True:
                    self.screen.blit(self.fireModel, (self.position.x,self.position.y-(32*i)))
                if contDown == True :
                    self.screen.blit(self.fireModel, (self.position.x,self.position.y+(32*i)))
                                 
    def initBomb(self,x,y,firePower,startTime):
        self.position.x = x
        self.position.y = y
        self.tickTime = 10
        self.firePower = firePower
        self.startTime = startTime

